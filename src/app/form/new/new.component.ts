import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connect } from '../../connection';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  url_ip = Connect.url;
  //user info
  user_id = localStorage.getItem('id');
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_position = localStorage.getItem('position');
  user_tel = localStorage.getItem('tel');
  user_roleid = localStorage.getItem('role_id');
  user_image_url = localStorage.getItem('image_url');
  user_image_type = localStorage.getItem('image_type');
  token_id = localStorage.getItem('token');

  //form
  nameForms: any;
  disForms: any;

  //image form
  formImage = [
    {
      path: "/assets/img/landscape.png",
      type: "image/png"
    }
  ];
  selectedFiles: File;

  //additem
  isAdditem: boolean = false;
  //item list
  myFormlist: number = 0;
  myForm = [];

  constructor(private modalService: NgbModal, private http: HttpClient) { }

  ngOnInit() {

  }

  handleFileInput(event) {
    this.formImage = [];
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          path: event.target.result,
          type: this.selectedFiles.type
        });
      }

      reader.readAsDataURL(this.selectedFiles);
      console.log(this.formImage);
    }
  }

  addItem() {
    this.myFormlist += 1;
    if (this.myFormlist != 0) {
      this.isAdditem = true;
      this.myForm.push({
        order_id: this.myFormlist,
        data_type: "String",
        title: "",
        description: "",
        required: true,
        nm_search: true,
        av_search: true
      });

    }
    console.log("list = " + this.myFormlist);
    console.log(this.myForm);
  }
  removeItem(index) {
    if (this.myFormlist > 0) {
      this.myFormlist -= 1;
      this.myForm.splice(index, 1);
    }
    console.log("remove list = " + this.myFormlist);
    console.log(this.myForm);
  }

  createForm() {
    //base64 image
    var imageText: any;
    let itemArray: any;

    for (let item of this.formImage) {
      if (item.type === "image/jpeg") {
        imageText = item.path.split(",")[1];
      }
      else if (item.type === "image/png") {
        imageText = item.path.split(",")[1];
      }
      else if (item.type === "video/mp4") {
        imageText = item.path.split(",")[1];
      }
      else { //null
        imageText = "";
      }

      itemArray = imageText;
    }

    let form_item: any = [];
    //formlist
    for (let item of this.myForm) {
      console.log(item);
      form_item.push(item);
    }

    let myForm = {
      form: {
        title: this.nameForms,
        description: this.disForms,
        image: itemArray,
      },
      form_item
    }
    console.log(myForm);
    this.http.post(this.url_ip + '/forms', myForm, {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      console.log(data);

    });
  }

}
