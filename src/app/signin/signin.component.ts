import { Component, OnInit, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MyserviceService } from './../myservice.service';
import { RouterModule, Router } from '@angular/router';
import { Connect } from '../connection';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: [MyserviceService]
})
export class SigninComponent implements OnInit {

  //ip info
  url_ip = Connect.url;

  //alert message
  alertMeg: String = "";

  constructor(private http: HttpClient, private service: MyserviceService, private routes: Router) { }

  ngOnInit() {
  }

  signin(username, password) {
    if (username == "" || password == "") {
      this.alertMeg = "username or password is empty.";
    } else {
      var login = {
        session: {
          username: username,
          password: password
        }
      }
      this.http.post(this.url_ip + "/login", login).subscribe((data) => {
        console.log(data);
        if (data['message'] != "Invalid username or password") {
          var image_url = this.url_ip + data['user_info']['image'].url;
          localStorage.setItem('id', data['user_info']['id']);
          localStorage.setItem('user_name', data['user_info']['username']);
          localStorage.setItem('name', data['user_info']['name']);
          localStorage.setItem('email', data['user_info']['email']);
          localStorage.setItem('position', data['user_info']['position']);
          localStorage.setItem('tel', data['user_info']['tel']);
          localStorage.setItem('role_id', data['user_info']['role_id']);
          localStorage.setItem('image_url', image_url);
          localStorage.setItem('image_type', data['user_info']['image'].content_type);
          localStorage.setItem('token', data['token']);
          this.alertMeg = "";

          this.routes.navigate(['/contents/dashboard']);
        } else {
          this.alertMeg = "Invalid username or password";
        }
      });

    }

  }



}
