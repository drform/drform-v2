import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { RouterModule, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
  //ip info
  url_ip = localStorage.getItem('ip');
  output: boolean = false;

  constructor(private http: HttpClient, private routes: Router) {

  }
  checkusernameandpassword(login) {


    var output: any = false;


    this.http.post(this.url_ip + "/login", login).subscribe((data) => {
      console.log(data);
      localStorage.setItem('id', data['user_info']['id']);
      localStorage.setItem('user_name', data['user_info']['username']);
      localStorage.setItem('name', data['user_info']['name']);
      localStorage.setItem('email', data['user_info']['email']);
      localStorage.setItem('position', data['user_info']['position']);
      localStorage.setItem('tel', data['user_info']['tel']);
      localStorage.setItem('role_id', data['user_info']['role_id']);
      localStorage.setItem('image_url', data['user_info']['image'].url);
      localStorage.setItem('image_type', data['user_info']['image'].content_type);
      output = true;
    });

    if (output == true) {
      return true;
    } else {
      return false;
    }
  }




}
