import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { Connect } from '../../connection';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  //ip info
  url_ip = Connect.url;
  //user info
  user_id = localStorage.getItem('id');
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_position = localStorage.getItem('position');
  user_tel = localStorage.getItem('tel');
  user_roleid = localStorage.getItem('role_id');
  user_image_url = localStorage.getItem('image_url');
  user_image_type = localStorage.getItem('image_type');
  token_id = localStorage.getItem('token');
  //myform
  myForm = [];

  constructor(private http: HttpClient, private routes: Router) { }


  ngOnInit() {
    console.log(this.token_id);
    this.getForm();
  }

  getForm() {
    this.myForm = [];
    this.http.get(this.url_ip + '/forms', {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      //console.log(data);
      var keys = Object.keys(data);
      var len = keys.length;

      if (len > 0) {
        for (let i = 0; i < len; i++) {
          this.myForm.push({
            id: data[i]['id'],
            title: data[i]['title'],
            description: data[i]['description'],
            user_id: data[i]['user_id'],
            image: [
              {
                url: this.url_ip + data[i]['image'].url,
                content_type: data[i]['image'].content_type
              }
            ],
            item: data[i]['item'],
            share: data[i]['share'],
          })
        }
        console.log(this.myForm);
      }

    });
  }

  logOut() {
    this.http.delete(this.url_ip + "/logout", {
      headers: new HttpHeaders({
        'token': this.token_id,
      })
    }).subscribe((data) => {
      console.log(data);

      localStorage.setItem('id', "");
      localStorage.setItem('user_name', "");
      localStorage.setItem('name', "");
      localStorage.setItem('email', "");
      localStorage.setItem('position', "");
      localStorage.setItem('tel', "");
      localStorage.setItem('role_id', "");
      localStorage.setItem('image_url', "");
      localStorage.setItem('image_type', "");
      localStorage.setItem('token', "");

      this.routes.navigate(['/signin']);


    });
  }

}
