import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  //ip info
  url_ip = localStorage.getItem('ip');

  //user info
  user_id = localStorage.getItem('id');
  user_username = localStorage.getItem('user_name');
  user_name = localStorage.getItem('name');
  user_email = localStorage.getItem('email');
  user_position = localStorage.getItem('position');
  user_tel = localStorage.getItem('tel');
  user_roleid = localStorage.getItem('role_id');
  user_image_url = localStorage.getItem('image_url');
  user_image_type = localStorage.getItem('image_type');
  token_id = localStorage.getItem('token');

  constructor(private routes: Router) { }

  ngOnInit() {
    console.log(this.user_image_url);
  }

  logOut() {
  }

}
