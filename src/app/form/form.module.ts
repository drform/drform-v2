import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormRoutingModule } from './form-routing.module';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { NavbarComponent } from '../layout/navbar/navbar.component';
import { FooterComponent } from '../layout/footer/footer.component';
import { ContentComponent } from '../layout/content/content.component';
import { NewComponent } from './new/new.component';
import { EditComponent } from './edit/edit.component';
import { ShowComponent } from './show/show.component';
import { ListComponent } from './list/list.component';
import { FormComponent } from '../form/form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [
    DashboardComponent,
    NavbarComponent,
    FooterComponent,
    ContentComponent,
    FormComponent,
    NewComponent,
    EditComponent,
    ShowComponent,
    ListComponent,
  ],
})
export class FormModule { }
